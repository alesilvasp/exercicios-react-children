import "./App.css";

import CenteredCard from "./Components/CenteredCards";

function App() {
  const children = "Não sou um botão!(ainda)";

  return (
    <div className="App">
      <div className="App-header">
        <div className="cardsContainer">
          <CenteredCard> {children} </CenteredCard>
          <CenteredCard> {children} </CenteredCard>
          <CenteredCard> {children} </CenteredCard>
        </div>
      </div>
    </div>
  );
}

export default App;
